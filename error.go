package rainforest

import (
	"fmt"
)

// HTTPError - specific http error
type HTTPError struct {
	Code    int
	Message string
}

// HTTPErrorHandler - interface to handle http errors
type HTTPErrorHandler interface {
	HandleHTTPError(err HTTPError)
}

func (e *HTTPError) Error() string {
	return fmt.Sprintf("%d - %s", e.Code, e.Message)
}

// NewHTTPError - creates a new http error
func NewHTTPError(code int, message string) HTTPError {
	return HTTPError{
		Code:    code,
		Message: message,
	}
}
