package rainforest

import "github.com/fatih/color"

// cBlack
var cBlack = color.New(color.FgBlack).SprintFunc()
var cHiBlack = color.New(color.FgHiBlack).SprintFunc()

// cBlue
var cBlue = color.New(color.FgBlue).SprintFunc()
var cHiBlue = color.New(color.FgHiBlue).SprintFunc()

// cMagenta
var cMagenta = color.New(color.FgMagenta).SprintFunc()
var cHiMagenta = color.New(color.FgHiMagenta).SprintfFunc()

// cCyan
var cCyan = color.New(color.FgCyan).SprintFunc()
var cHiCyan = color.New(color.FgHiCyan).SprintFunc()

// cRed
var cRed = color.New(color.FgRed).SprintFunc()
var cHiRed = color.New(color.FgHiRed).SprintFunc()

// cYellow
var cYellow = color.New(color.FgYellow).SprintFunc()
var cHiYellow = color.New(color.FgHiYellow).SprintFunc()

// cGreen
var cGreen = color.New(color.FgGreen).SprintfFunc()
var cHiGreen = color.New(color.FgHiGreen).SprintFunc()
