package rainforest

import (
	"errors"
	"sync"
)

// MaxMemoryUsage - if cache CacheContext hit this threshold, must free last hitted data
var MaxMemoryUsage = int64(0x40000000) // 1G

// Memory stores cache data
type Memory struct {
	HitCount int64
	Data     []byte
}

// CacheContext stores cache CacheContext data
type CacheContext struct {
	MemoryUsage int64
	InMemory    map[string]*Memory
	Mutex       sync.Mutex
}

// Contains - checks if content exists in cache
func (cache *CacheContext) Contains(key string) (result bool) {

	cache.Mutex.Lock()

	_, result = cache.InMemory[key]

	cache.Mutex.Unlock()

	return result
}

// Get - get content by name
func (cache *CacheContext) Get(key string) (content []byte, err error) {

	cache.Mutex.Lock()

	if mem, ok := cache.InMemory[key]; ok {
		content = mem.Data
		mem.HitCount++
	} else {
		err = errors.New("content not found")
	}

	cache.Mutex.Unlock()

	return
}

// Set - set content
func (cache *CacheContext) Set(key string, content []byte) {

	cache.Mutex.Lock()

	cache.fitMemory()

	if mem, ok := cache.InMemory[key]; ok {
		mem.HitCount++
		mem.Data = content
	} else {
		cache.InMemory[key] = &Memory{
			HitCount: 0,
			Data:     content,
		}
	}

	cache.Mutex.Unlock()
}

func (cache *CacheContext) fitMemory() {

	if cache.MemoryUsage > MaxMemoryUsage {

		lowerHitMemory := ""
		lowerHit := int64(0xffffffff)

		for name, mem := range cache.InMemory {

			if mem.HitCount < lowerHit {
				lowerHitMemory = name
			}
		}

		if len(lowerHitMemory) > 0 {
			delete(cache.InMemory, lowerHitMemory)
		}
	}
}

// New - instantiate new cache
func New() *CacheContext {
	return &CacheContext{
		InMemory: make(map[string]*Memory),
		Mutex:    sync.Mutex{},
	}
}

// GlobalCache - default cache memory
var GlobalCache = New()
