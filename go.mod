module gitlab.com/stepuplabs/go/rainforest

go 1.13

require (
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/fatih/color v1.7.0
	github.com/go-delve/delve v1.3.2 // indirect
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/google/uuid v1.1.1
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/peterh/liner v1.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/urfave/cli v1.22.1
	gitlab.com/stepuplabs/go/config v1.0.5

	gitlab.com/stepuplabs/go/crypt v1.0.1
	gitlab.com/stepuplabs/go/sessions v1.0.2
	go.starlark.net v0.0.0-20200203144150-6677ee5c7211 // indirect
	golang.org/x/arch v0.0.0-20191126211547-368ea8f32fff // indirect
	golang.org/x/crypto v0.0.0-20200204104054-c9f3fb736b72 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
