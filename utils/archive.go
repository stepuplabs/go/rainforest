package utils

import (
	"archive/tar"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// TarFile file helper
type TarFile struct {
	writer *tar.Writer
}

// NewTarFile creates tar struct
func NewTarFile(output io.Writer) *TarFile {
	return &TarFile{
		writer: tar.NewWriter(output),
	}
}

// AddFile adds a file to archive
func (tf *TarFile) AddFile(filename string) (err error) {

	var src *os.File

	if tf == nil || tf.writer == nil {
		err = errors.New("invalid tar file reference")
		return
	}

	src, err = os.Open(filename)

	if err != nil {
		return
	}
	defer src.Close()

	content, err := ioutil.ReadAll(src)

	if err != nil {
		return
	}

	header := &tar.Header{
		Name: filename,
		Mode: 0600,
		Size: int64(len(content)),
	}

	err = tf.writer.WriteHeader(header)

	if err != nil {
		return
	}

	_, err = tf.writer.Write(content)

	return
}

// AddAll adds directory content recursively to tar file
func (tf *TarFile) AddAll(directory string) (err error) {

	var stats []os.FileInfo

	stats, err = ioutil.ReadDir(directory)

	for _, item := range stats {

		if item.IsDir() {
			tf.AddAll(fmt.Sprintf("%s/%s", directory, item.Name()))
		} else {
			tf.AddFile(fmt.Sprintf("%s/%s", directory, item.Name()))
		}
	}

	return
}

// Close file
func (tf *TarFile) Close() {
	if tf.writer != nil {
		tf.writer.Close()
	}
}
