package rainforest

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/stepuplabs/go/config"
)

// HTTPServerDelegate - interface to handle server requests and error
type HTTPServerDelegate interface {
	HTTPErrorHandler // func HandleHTTPError()
	HandleHTTPRequest(w http.ResponseWriter, r *http.Request)
}

// HTTPServer - do the http tasks
type HTTPServer struct {
	http.Server

	UseSSL  bool
	SSLCert string
	SSLKey  string

	Delegate HTTPServerDelegate
}

// Initialize - Component.Initialize implementation
func (server *HTTPServer) Initialize(cs *config.Set) (err error) {

	server.Addr, err = cs.GetString("server.listen")

	if err != nil {
		return
	}

	server.ReadTimeout, err = cs.GetDuration("server.read_timeout")

	if err != nil {
		return
	}

	server.WriteTimeout, err = cs.GetDuration("server.write_timeout")

	if err != nil {
		return
	}

	server.MaxHeaderBytes, err = cs.GetInt("server.max_header_bytes")

	if err != nil {
		return
	}

	server.UseSSL, _ = cs.GetBool("server.use_ssl")

	if server.UseSSL {

		server.SSLCert, err = cs.GetString("server.ssl_cert")

		if err != nil {
			return
		}

		server.SSLKey, err = cs.GetString("server.ssl_key")

		if err != nil {
			return
		}
	}

	server.ReadTimeout *= time.Second
	server.WriteTimeout *= time.Second

	return
}

// Run - Component.Run implementation
func (server *HTTPServer) Run(action string, args ...string) (err error) {

	if action == "start" {
		server.start()
	}

	return
}

// Run - run server until interrupt signal
func (server *HTTPServer) start() {

	server.Handler = server

	curPID := os.Getpid()
	sigch := make(chan os.Signal, 1)

	signal.Notify(sigch, os.Interrupt)

	go func(parentPID int) {

		var err error

		fmt.Printf("[server] settings:\n")
		fmt.Printf("[server]   - server.addr..............: %s\n", server.Addr)
		fmt.Printf("[server]   - server.read_timeout......: %ds\n", int64(server.ReadTimeout/time.Second))
		fmt.Printf("[server]   - server.write_timeout.....: %ds\n", int64(server.WriteTimeout/time.Second))
		fmt.Printf("[server]   - server.max_header_bytes..: %d\n", server.MaxHeaderBytes)
		fmt.Printf("[server]   - server.UseSSL............: %v\n", server.UseSSL)

		if server.UseSSL {
			fmt.Printf("[server]     - server.SSLCert.........: %s\n", server.SSLCert)
			fmt.Printf("[server]     - server.SSLKey..........: %s\n", server.SSLKey)
		}

		fmt.Printf("\n[server] starting...\n")

		if server.UseSSL {
			err = server.ListenAndServeTLS(server.SSLCert, server.SSLKey)
		} else {
			err = server.ListenAndServe()
		}

		if err != nil && server.Delegate != nil {
			server.Delegate.HandleHTTPError(NewHTTPError(501, err.Error()))

			parentProc, err := os.FindProcess(parentPID)

			if err == nil {
				parentProc.Signal(os.Interrupt)
			}
		}
	}(curPID)

	// wait for signal
	<-sigch

	// shutdown server

	server.Shutdown(context.TODO())
}

func (server *HTTPServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if server.Delegate != nil {
		server.Delegate.HandleHTTPRequest(w, r)
	}
}
