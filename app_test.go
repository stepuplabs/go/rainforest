package rainforest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"
)

type TestController struct {
	*Controller
}

func (ctrl *TestController) BeforeAction(action string) (status int) {

	ctrl.Context.Header["Endpoint"] = action

	if action == "EndpointC" {
		ctrl.Redirect("/new_path")
		return http.StatusTemporaryRedirect
	}

	return http.StatusOK
}

func (ctrl *TestController) EndpointA(paramS string, paramI int) {
	ctrl.RenderHTML(http.StatusOK, "%s::%d", paramS, paramI)
}

func (ctrl *TestController) EndpointB() {
	ctrl.RenderJSON(http.StatusOK, H{
		"name":    "rainforest",
		"version": "2.0.0",
	})
}

func (ctrl *TestController) EndpointC() {
	ctrl.Done() // do nothing
}

func TestMain(t *testing.T) {

	os.Args = []string{"app", "server", "start"}

	go func() {

		app := NewApp("rftest", "2.0.0")

		app.GET("/a/:params/:parami", TestController{}, "EndpointA")
		app.GET("/b", TestController{}, "EndpointB")
		app.GET("/c", TestController{}, "EndpointC")

		err := app.Run()

		if err != nil {
			fmt.Printf("Failed to run test app: %v\n", err)
		}
	}()

	time.Sleep(2 * time.Second)

	checkEndpointA(t)
	checkEndpointB(t)
	checkEndpointC(t)
}

func checkEndpointA(t *testing.T) {

	paramS := "rainforest"
	paramI := 2
	expectedResult := fmt.Sprintf("%s::%d", paramS, paramI)

	resp, err := http.Get(fmt.Sprintf("http://localhost:3000/a/%s/%d", paramS, paramI))

	if err != nil {
		t.Error(err)
		return
	}
	defer resp.Body.Close()

	text, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		t.Error(err)
		return
	}

	result := string(text)

	if result != expectedResult {
		t.Error(fmt.Errorf("expected \"%s\" as a result of EndpointA, but was returned \"%s\"",
			expectedResult,
			result))
		return
	}

	if action := resp.Header.Get("Endpoint"); action != "EndpointA" {
		t.Error(fmt.Errorf("invalid Endpoint Header. Expected EndpointA but got %s", action))
	}
}

func checkEndpointB(t *testing.T) {

	var result struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	}

	expectedName := "rainforest"
	expectedVersion := "2.0.0"

	resp, err := http.Get("http://localhost:3000/b")

	if err != nil {
		t.Error(err)
		return
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	err = decoder.Decode(&result)

	if err != nil {
		t.Error(err)
		return
	}

	if result.Name != expectedName {
		t.Error(fmt.Errorf("invalid result.Name %s != %s", result.Name, expectedName))
		return
	}

	if result.Version != expectedVersion {
		t.Error(fmt.Errorf("invalid result.Version %s != %s", result.Version, expectedVersion))
	}

	if action := resp.Header.Get("Endpoint"); action != "EndpointB" {
		t.Error(fmt.Errorf("invalid Endpoint Header. Expected EndpointB but got %s", action))
	}
}

func checkEndpointC(t *testing.T) {

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	resp, err := client.Get("http://localhost:3000/c")

	if err != nil {
		t.Error(err)
		return
	}
	defer resp.Body.Close()

	if path := resp.Header.Get("Location"); path != "/new_path" {
		t.Error(fmt.Errorf("invalid Location Header: %s != /new_path", path))
	}

	if action := resp.Header.Get("Endpoint"); action != "EndpointC" {
		t.Error(fmt.Errorf("invalid Endpoint Header. Expected EndpointC but got %s", action))
	}
}
