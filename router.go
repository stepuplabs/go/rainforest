package rainforest

import (
	"fmt"
	"log"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

// RouteActionParam represents a controller action parameter
type RouteActionParam struct {
	name  string
	value string
}

// RouteAction contains information to execute a controller action
type RouteAction struct {
	name   string
	typ    reflect.Type
	params []RouteActionParam
}

// NodeType is one of the types a route node can be
type NodeType int

const (
	// RootNodeType is the root node
	RootNodeType NodeType = 0

	// PathNodeType is part of the url path
	PathNodeType NodeType = 1

	// ParamNodeType represents a route parameter in url path
	ParamNodeType NodeType = 2
)

// RouteNode - one node of a Route
type RouteNode struct {
	method   string
	path     string
	name     string
	nodeType NodeType
	action   *RouteAction
	parent   *RouteNode
	childs   []*RouteNode
}

func (node *RouteNode) find(nodeName string) (foundNode *RouteNode) {

	var paramNode *RouteNode
	foundNode = nil

	for _, node := range node.childs {

		if node.name == nodeName {
			foundNode = node
			break
		} else if node.nodeType == ParamNodeType {
			paramNode = node
		}
	}

	if foundNode == nil {
		foundNode = paramNode
	}

	return
}

func newRouteNode(method string, path string, name string, parent *RouteNode) *RouteNode {

	var nodeType NodeType

	if strings.Index(name, ":") == 0 {
		nodeType = ParamNodeType
	} else {
		nodeType = PathNodeType
	}

	node := &RouteNode{
		method:   method,
		path:     path,
		name:     name,
		nodeType: nodeType,
		action:   nil,
		parent:   parent,
		childs:   make([]*RouteNode, 0),
	}

	return node
}

func newRootRouteNode(host string) *RouteNode {

	return &RouteNode{
		name:     host,
		nodeType: RootNodeType,
		action:   nil,
		childs:   make([]*RouteNode, 0),
	}
}

// Router - representes a path route
type Router struct {
	host     string
	rootNode *RouteNode
}

// NewRouter - creates a new router
func NewRouter(host string) *Router {
	return &Router{
		host:     host,
		rootNode: newRootRouteNode(host),
	}
}

// GET - AddRoute("GET")
func (router *Router) GET(path string, controller interface{}, action string) {
	router.AddRoute("GET", path, controller, action)
}

// POST - AddRoute("POST")
func (router *Router) POST(path string, controller interface{}, action string) {
	router.AddRoute("POST", path, controller, action)
}

// PUT - AddRoute("PUT")
func (router *Router) PUT(path string, controller interface{}, action string) {
	router.AddRoute("PUT", path, controller, action)
}

// DELETE - AddRoute("DELETE")
func (router *Router) DELETE(path string, controller interface{}, action string) {
	router.AddRoute("DELETE", path, controller, action)
}

// AddRoute - add a new node to Router
func (router *Router) AddRoute(method string, path string, controller interface{}, action string) {

	pathParts := buildPath(method, path)

	if router.rootNode == nil {
		router.rootNode = newRootRouteNode(router.host)
	}

	pNode := &router.rootNode
	params := make([]RouteActionParam, 0)

	for _, part := range pathParts {

		var foundNode **RouteNode

		for idx, n := range (*pNode).childs {
			if n.name == part {
				foundNode = &(*pNode).childs[idx]
				break
			}
		}

		if foundNode == nil {
			(*pNode).childs = append((*pNode).childs, newRouteNode(method, path, part, *pNode))

			length := len((*pNode).childs)

			pNode = &(*pNode).childs[length-1]
		} else {
			pNode = foundNode
		}

		if (*pNode).nodeType == ParamNodeType {
			params = append(params, RouteActionParam{
				name:  (*pNode).name[1:],
				value: "",
			})
		}
	} // end of for

	endpoints := make([]*RouteNode, 1)
	endpoints[0] = *pNode

	for _, pp := range (*pNode).parent.childs {
		if (pp.nodeType == ParamNodeType && pp.action != nil || (pp.nodeType == PathNodeType && pp.name == (*pNode).name)) && pp.action != nil {
			endpoints = append(endpoints, pp)
		}
	}

	(*pNode).action = &RouteAction{
		typ:    reflect.TypeOf(controller),
		name:   action,
		params: params,
	}

	if len(endpoints) > 1 {
		log.Printf("[%s]  One of the routes above can have no effect, beacause there is more than one action in the same leaf:\n", cYellow("WARNING"))

		for idx, pr := range endpoints {
			log.Printf("  * [%d] %s %s -> %s.%s()\n", idx, cGreen(pr.method), cRed(pr.path), cHiBlack(reflect.TypeOf(controller).Name()), cHiBlack(pr.action.name))
		}

		log.Printf("\n")
	}
}

func printNode(routeNode *RouteNode) {
	switch routeNode.nodeType {
	case RootNodeType:
		fmt.Printf("* [%s]", cYellow(routeNode.name))
	case ParamNodeType:
		fmt.Printf("|- %s ", cMagenta(routeNode.name))
	case PathNodeType:
		fmt.Printf("|- %s ", cGreen(routeNode.name))
	}

	if routeNode.action != nil {
		fmt.Printf(">> %s.%s(", cHiBlack(routeNode.action.typ.Name()), cHiBlack(routeNode.action.name))

		for idx, param := range routeNode.action.params {

			if idx > 0 {
				fmt.Printf(", ")
			}

			fmt.Printf("%s", param.name)
		}

		fmt.Printf(")\n")

	} else {
		fmt.Printf("\n")
	}
}

// Route - find action that match with context path and run it
func (router *Router) Route(ctx *Context) (routed bool) {

	status := 200

	pathParts := buildPath(ctx.Request.Method, ctx.Request.URL.Path)

	params := make([]string, 0)
	pNode := router.rootNode

	for _, part := range pathParts {

		if foundNode := pNode.find(part); foundNode != nil {
			pNode = foundNode

			if pNode.nodeType == ParamNodeType {
				params = append(params, part)
			}
		} else {
			pNode = nil
			break
		}
	}

	if pNode != nil && pNode.action != nil {

		for i, param := range params {
			pNode.action.params[i].value = param
		}

		status = router.run(ctx, pNode.action)

		log.Printf("[%s] %s %s%s %s (%s) -> %s.%s() ->  [%d]\n",
			cHiBlue(ctx.App.Name),
			cBlue(ctx.Request.Method),
			ctx.Request.Host,
			cGreen(ctx.Request.URL.Path),
			cBlue(ctx.Request.Proto),
			cCyan(router.remoteAddr(ctx)),
			cHiBlack(pNode.action.typ.Name()),
			cHiBlack(pNode.action.name),
			status)

		routed = true // routed successfully
	} else { // not found
		status = http.StatusNotFound

		log.Printf("[%s] %s %s%s %s (%s) -> No route to path ->  [%d]\n",
			cHiBlue(ctx.App.Name),
			cBlue(ctx.Request.Method),
			ctx.Request.Host,
			cGreen(ctx.Request.URL.Path),
			cBlue(ctx.Request.Proto),
			cCyan(router.remoteAddr(ctx)),
			status)

		routed = false // no route found
	}

	return
}

func (router *Router) run(ctx *Context, action *RouteAction) (status int) {

	dispatcher := NewActionDispatcher(ctx, action)

	return dispatcher.run()
}

func (router *Router) remoteAddr(ctx *Context) string {
	addresses := make([]string, 1)

	addresses[0] = ctx.Request.RemoteAddr

	if addr := ctx.Request.Header.Get("X-Forwarded-For"); len(addr) > 0 {
		addresses = append(addresses, addr)
	}

	if addr := ctx.Request.Header.Get("X-Real-IP"); len(addr) > 0 {
		addresses = append(addresses, addr)
	}

	return strings.Join(addresses, "/")
}

// RouteTree - prints the router tree
func (router *Router) RouteTree() {
	fmt.Printf("\n")
	printTree(router.rootNode, 0)
}

func printTree(routeNode *RouteNode, dep int) {

	pNode := routeNode

	for i := 0; i < dep; i++ {
		fmt.Printf("   ")
	}

	printNode(routeNode)

	for _, child := range pNode.childs {
		printTree(child, dep+1)
	}
}

func replace(src string, exp string, to string) (result string) {

	re, err := regexp.Compile(exp)

	if err != nil {
		result = src
	} else {
		result = string(re.ReplaceAll([]byte(src), []byte(to)))
	}

	return
}

func buildPath(method string, path string) []string {

	parts := make([]string, 2)
	parts[0] = strings.ToLower(method)
	parts[1] = "/"

	sanitizedPath := sanitizePath(path)

	for _, part := range strings.Split(sanitizedPath, "/") {

		trimmed := strings.TrimSpace(part)

		if len(trimmed) > 0 {
			parts = append(parts, trimmed)
		}

	}

	return parts
}

func sanitizePath(path string) (result string) {

	result = path

	result = replace(result, "\\.\\.+", "")
	result = replace(result, "//+", "/")
	result = replace(result, "\\.\\.+", ".")

	return
}

func reflectValue(sval string, typ reflect.Type) (value reflect.Value, err error) {

	switch typ.Name() {
	case "string":
		value = reflect.ValueOf(sval)
	case "int":
		ival, e := strconv.Atoi(sval)

		if e == nil {
			value = reflect.ValueOf(ival)
		} else {
			err = e
			value = reflect.ValueOf(-1)
			log.Printf("err: %s\n", err.Error())
		}

	case "uint64":
		uval, e := strconv.ParseUint(sval, 0, 64)

		if e == nil {
			value = reflect.ValueOf(uval)
		} else {
			err = e
			value = reflect.ValueOf(0)
		}

	case "bool":
		bval, e := strconv.ParseBool(sval)

		if e == nil {
			value = reflect.ValueOf(bval)
		} else {
			err = e
			value = reflect.ValueOf(false)
		}

	case "float64":
		fval, e := strconv.ParseFloat(sval, 64)

		if e == nil {
			value = reflect.ValueOf(fval)
		} else {
			err = e
			value = reflect.ValueOf(0.0)
		}
	} // end of switch

	return
}
