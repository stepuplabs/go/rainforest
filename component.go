package rainforest

import "gitlab.com/stepuplabs/go/config"

// Component is anything useful for application
type Component interface {
	Initialize(cs *config.Set) error
	Run(action string, args ...string) error
}
