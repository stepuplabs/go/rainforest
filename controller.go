package rainforest

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gitlab.com/stepuplabs/go/crypt"
)

// Controller - represents an application controller
type Controller struct {
	Context *Context
}

// NewController - instantiate a new base controller
func NewController(context *Context) *Controller {
	return &Controller{Context: context}
}

// Request - getter for http request
func (ctrl *Controller) Request() *http.Request {
	return ctrl.Context.Request
}

// QueryString - get query string value
func (ctrl *Controller) QueryString(name string) string {
	return ctrl.Context.Request.URL.Query().Get(name)
}

// Redirect - redirects to another url
func (ctrl *Controller) Redirect(url string) {
	ctrl.Context.Redirect(url)
}

// RenderHTML - Print html content
func (ctrl *Controller) RenderHTML(status int, html string, args ...interface{}) {
	ctrl.Context.RenderHTML(status, html, args...)
}

// ParseTemplate - parses template e returns processed string
func (ctrl *Controller) ParseTemplate(file string, obj interface{}) string {

	var err error
	var buff bytes.Buffer

	t := template.New(path.Base(file))
	t = t.Funcs(template.FuncMap{
		"safeHTML": func(b string) template.HTML {
			return template.HTML(b)
		},

		"safeJS": func(b string) template.JS {
			return template.JS(b)
		},

		"dateFormat": func(layout string, timestamp time.Time) string {
			return timestamp.Format(layout)
		},
	})

	t, err = t.ParseFiles(file)

	if err == nil {

		e := t.Execute(&buff, obj)

		if e != nil {
			log.Printf("Failed to execute template: %s\n", e.Error())
		}
	} else {
		log.Printf("ERROR: %s\n", err.Error())
	}

	return buff.String()
}

// RenderFile - Print content of file
func (ctrl *Controller) RenderFile(filename string) {

	fileInfoHash, err := ctrl.readLocalFileMetaInfoHash(filename)

	if err != nil {
		ctrl.RenderError(404, errors.New("not found"))
		return
	}

	requestedInfoHash := ctrl.Request().Header.Get("If-None-Match")

	/* If Browser Cache is up to date, send 304 response */

	if len(requestedInfoHash) > 0 && requestedInfoHash == fileInfoHash {

		ctrl.Context.Header["Etag"] = fileInfoHash
		ctrl.Context.Header["Cache-Control"] = "max-age=31557600"

		ctrl.Context.SendHeader(http.StatusNotModified)

		return
	}

	/* Check if file exists in global cache */

	content, err := GlobalCache.Get(fileInfoHash)

	if err != nil {

		/* If content is not found in global cache, read it from disk */

		content, err = ctrl.readLocalFile(filename)

		/* If content is available, saves in global cache */

		if err == nil {
			GlobalCache.Set(fileInfoHash, content)
		}
	}

	if err != nil {
		ctrl.RenderError(http.StatusNotFound, errors.New("Not found"))
		return
	}

	ctrl.Context.Header["Accept-Ranges"] = "none"
	ctrl.Context.Header["Etag"] = fileInfoHash
	ctrl.Context.Header["Cache-Control"] = "max-age=31557600"

	if ctrl.Context.Request.Method != "HEAD" {
		ctrl.Context.Respond(http.StatusOK, ctrl.getContentType(filename, content), content)
	}
}

// RenderFileAsAttachment - Print content of file
func (ctrl *Controller) RenderFileAsAttachment(filename string) {

	content, err := ctrl.readLocalFile(filename)

	if err != nil {
		ctrl.RenderError(http.StatusNotFound, errors.New("Not found"))
		return
	}

	ctrl.Context.Header["Content-Disposition"] = fmt.Sprintf("attachment; filename=\"%s\"", filepath.Base(filename))
	ctrl.Context.Header["Accept-Ranges"] = "none"

	if ctrl.Context.Request.Method != "HEAD" {
		ctrl.Context.Respond(http.StatusOK, ctrl.getContentType(filename, content), content)
	}
}

// JSONObject - Unmarshal json content
func (ctrl *Controller) JSONObject(object interface{}) (err error) {

	req := ctrl.Request()

	err = nil

	if req.ContentLength == 0 {
		err = errors.New("empty body")
	} else {

		body, e := ioutil.ReadAll(req.Body)

		if e == nil {
			err = json.Unmarshal(body, object)
		} else {
			err = e
		}
	}

	return
}

// RenderJSON - Renders json data
func (ctrl *Controller) RenderJSON(status int, obj interface{}) {
	ctrl.Context.RenderJSON(status, obj)
}

// RenderJSONError - generate error response in json format
func (ctrl *Controller) RenderJSONError(status int, err error) {
	ctrl.RenderJSON(status, H{
		"error_code":    status,
		"error_message": err.Error(),
	})
}

// RenderError send error response
func (ctrl *Controller) RenderError(status int, err error) {

	contentType := ctrl.Request().Header.Get("Accept")

	if len(contentType) == 0 {
		contentType = ctrl.Request().Header.Get("Content-Type")
	}

	switch {
	case strings.Contains(contentType, "application/json"):
		ctrl.RenderJSONError(status, err)
	case strings.Contains(contentType, "application/xml"):
		ctrl.RenderXMLError(status, err)
	default:
		ctrl.RenderHTML(status, "<h1>%d - %s</h1>", status, err.Error())
	}
}

// Done - return http status 204 - No Content
func (ctrl *Controller) Done() {
	ctrl.Context.SendHeader(http.StatusNoContent)
}

// XMLObject - Unmarshal xml content
func (ctrl *Controller) XMLObject(object interface{}) (err error) {

	req := ctrl.Request()
	contentType := req.Header.Get("Content-Type")

	err = nil

	if req.ContentLength == 0 || !strings.Contains(contentType, "application/xml") {
		err = errors.New("There is no json object available")
	} else {

		body, e := ioutil.ReadAll(req.Body)

		if e == nil {
			err = xml.Unmarshal(body, object)
		} else {
			err = e
		}
	}

	return
}

// RenderXML - Renders xml data
func (ctrl *Controller) RenderXML(status int, obj interface{}) {
	ctrl.Context.RenderXML(status, obj)
}

// RenderXMLError - generate error response in xml format
func (ctrl *Controller) RenderXMLError(status int, err error) {
	ctrl.Context.RenderXML(status, H{
		"error_status":  status,
		"error_message": err.Error(),
	})
}

/*
 * Auxiliar Functions
 */

func (ctrl *Controller) readLocalFile(filename string) (content []byte, err error) {

	file, err := os.Open(filename)

	if err != nil {
		return
	}
	defer file.Close()

	content, err = ioutil.ReadAll(file)

	return
}

func (ctrl *Controller) readLocalFileMetaInfoHash(filename string) (metaInfoHash string, err error) {

	file, err := os.Open(filename)

	if err != nil {
		return
	}
	defer file.Close()

	stats, err := file.Stat()

	if err != nil {
		return
	}

	metaInfo := fmt.Sprintf("name=%s,modified=%d", filename, stats.ModTime().Unix())
	metaInfoHash = crypt.MD5([]byte(metaInfo))

	return
}

func (ctrl *Controller) getContentType(filename string, content []byte) (contentType string) {

	contentType = "text/plain"

	switch {
	case strings.Contains(filename, ".html"):
		contentType = "text/html"
	case strings.Contains(filename, ".js"):
		contentType = "text/javascript"
	case strings.Contains(filename, ".css"):
		contentType = "text/css"
	default:
		contentType = http.DetectContentType(content)
	}

	return
}

func (ctrl *Controller) findHTMLResources(content []byte) []string {

	resources := make([]string, 0)

	re := regexp.MustCompile("<(link|script).*(href|src)=\"([a-zA-Z./-_]+)\".*>")

	matchs := re.FindAllSubmatch(content, -1)

	if matchs != nil {
		for _, res := range matchs {
			if len(res) == 4 {

				resourcePath := fmt.Sprintf("%s/%s", filepath.Dir(os.Args[0]), string(res[3]))

				if _, err := os.Stat(resourcePath); err == nil {
					resources = append(resources, resourcePath)
				}
			}
		}
	}

	return resources
}
