package rainforest

import (
	"log"
	"net/http"
	"reflect"
)

// ActionDispatcher - dispatcher a controller action
type ActionDispatcher struct {
	ctx    *Context
	action *RouteAction
}

func (disp *ActionDispatcher) run() (status int) {

	ctx := disp.ctx
	action := disp.action
	status = http.StatusOK

	actionCtrl := instantiateType(action.typ)

	ctxField := actionCtrl.Elem().FieldByName("Context")

	if ctxField.IsValid() {
		ctxField.Set(reflect.ValueOf(ctx))
	} else {
		panic(cRed("Trying to call a struct that is not a Controller."))
	}

	method := actionCtrl.MethodByName(action.name)

	if !method.IsValid() {
		log.Printf("[%s] %s.%s() not found.\n", cRed("error"), cHiBlack(action.typ.Name()), cHiBlack(action.name))
		status = http.StatusInternalServerError
		return
	}

	numArgs := method.Type().NumIn()

	if numArgs != len(action.params) {
		status = http.StatusInternalServerError
		return
	}

	methodArgs := make([]reflect.Value, numArgs)

	for i := 0; i < numArgs; i++ {

		in := method.Type().In(i)

		value, e := reflectValue(action.params[i].value, in)

		if e != nil {
			status = http.StatusBadRequest
			return
		}

		methodArgs[i] = value
	}

	// run BeforeAction method if exists

	if status == http.StatusOK {
		status = runBeforeAction(actionCtrl, action.name)
	}

	if status == http.StatusOK {
		method.Call(methodArgs)
		runPostAction(actionCtrl, action.name)
	}

	return
}

func runBeforeAction(ctrl reflect.Value, action string) (status int) {

	status = http.StatusOK

	method := ctrl.MethodByName("BeforeAction")

	if !method.IsValid() {
		return
	}

	methodParam := method.Type().In(0)

	if methodParam.Kind() != reflect.String {
		return
	}

	args := make([]reflect.Value, 1)
	args[0] = reflect.ValueOf(action)

	result := method.Call(args)

	if len(result) == 1 && result[0].Kind() == reflect.Int {
		status = int(result[0].Int())
	}

	return
}

func runPostAction(ctrl reflect.Value, action string) {

	method := ctrl.MethodByName("PostAction")

	if !method.IsValid() {
		return
	}

	methodParam := method.Type().In(0)

	if methodParam.Kind() != reflect.String {
		return
	}

	args := make([]reflect.Value, 1)
	args[0] = reflect.ValueOf(action)

	method.Call(args)

	return
}

func instantiateType(typ reflect.Type) reflect.Value {

	var val *reflect.Value

	principal := reflect.New(typ)

	val = &principal

	for reflect.Indirect(*val).Kind() == reflect.Struct && reflect.Indirect(*val).NumField() > 0 {

		var newTyp reflect.Type

		firstField := reflect.Indirect(*val).Field(0)

		if firstField.Kind() != reflect.Ptr || !firstField.IsNil() {
			val = &firstField
			continue
		}

		newTyp = firstField.Type().Elem()

		newVal := reflect.New(newTyp)

		firstField.Set(newVal)

		val = &newVal
	}

	return principal
}

// NewActionDispatcher - creates a new action dispatcher
func NewActionDispatcher(ctx *Context, action *RouteAction) *ActionDispatcher {
	return &ActionDispatcher{ctx: ctx, action: action}
}
