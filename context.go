package rainforest

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
)

// Context - represents a request context
type Context struct {
	W       http.ResponseWriter
	Request *http.Request
	App     *App
	Header  map[string]string
}

// SendHeader - send response header
func (ctx *Context) SendHeader(status int) {

	for k, v := range ctx.Header {
		ctx.W.Header().Set(k, v)
	}

	ctx.W.WriteHeader(status)
}

// Respond - send response
func (ctx *Context) Respond(status int, contentType string, content []byte) {

	ctx.Header["Content-Type"] = contentType

	ctx.SendHeader(status)
	ctx.W.Write(content)
}

// RenderString - Write string to response body
func (ctx *Context) RenderString(status int, format string, args ...interface{}) {
	ctx.Respond(status, "text/plain", []byte(fmt.Sprintf(format, args...)))
}

// RenderHTML - send response as html
func (ctx *Context) RenderHTML(status int, content string, args ...interface{}) {
	ctx.Respond(status, "text/html", []byte(fmt.Sprintf(content, args...)))
	return
}

// RenderJSON - send response in json format
func (ctx *Context) RenderJSON(status int, obj interface{}) {

	content, err := json.Marshal(obj)

	if err != nil {
		ctx.RenderJSON(http.StatusInternalServerError, H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.Respond(status, "application/json", content)
}

// RenderXML - Send response in xml format
func (ctx *Context) RenderXML(status int, obj interface{}) {

	content, err := xml.Marshal(obj)

	if err != nil {
		ctx.RenderXML(http.StatusInternalServerError, H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.Respond(status, "application/json", content)
}

// Redirect - return 301
func (ctx *Context) Redirect(url string) {
	ctx.Header["Location"] = url
	ctx.SendHeader(http.StatusFound)
}

// NewContext - instantiate a new Context
func NewContext(app *App, w http.ResponseWriter, r *http.Request) *Context {
	return &Context{
		W:       w,
		Request: r,
		App:     app,
		Header:  make(map[string]string),
	}
}
