package rainforest

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/stepuplabs/go/config"
)

// App - represents a web appliation
type App struct {
	Name    string             // application name
	Version string             // binary version
	Config  *config.Set        // app.Configuration set
	server  *HTTPServer        // application http server
	routers map[string]*Router // specialized routers

	// flags
	fConfigFile      string
	fApplyMigrations bool

	components map[string]Component

	fValidInstance bool
}

// NewApp creates a new app
func NewApp(name string, version string) *App {

	app := &App{
		Name:           name,
		Version:        version,
		Config:         config.NewSet(),
		routers:        make(map[string]*Router),
		fConfigFile:    "config.yml",
		components:     make(map[string]Component),
		fValidInstance: true, // force to use NewApp method
	}

	return app
}

// AddComponent adds a component to application
func (app *App) AddComponent(name string, component Component) {
	app.components[name] = component
}

// Component returns a component by name if exists
func (app *App) Component(name string) (component Component, err error) {

	if c, ok := app.components[name]; ok {
		component = c
	} else {
		err = fmt.Errorf("component not found: %s", name)
	}

	return
}

// Initialize all components
func (app *App) initialize() (err error) {

	if !app.fValidInstance {
		err = errors.New("Invalid rainforest.App instance. Use rainforest.NewApp() method to create a valid one")
		return
	}

	app.Config, err = config.Load(app.fConfigFile)

	if err != nil {
		return
	}

	for name, comp := range app.components {

		err = comp.Initialize(app.Config)

		if err != nil {
			err = fmt.Errorf("failed to initialize application component: %s => %s", name, err.Error())
			break
		}
	}

	return
}

// execute application
func (app *App) exec() {

	if len(os.Args) <= 2 {
		app.printUsage()
		return
	}

	componentName := os.Args[1]
	command := os.Args[2]
	args := os.Args[3:]

	if component, ok := app.components[componentName]; ok {

		err := component.Run(command, args...)

		if err != nil {
			log.Printf("[%s][%s][ERROR] %s\n", app.Name, componentName, err.Error())
		}

	} else {

		log.Printf("[%s][ERROR] Component not found: %s\n", app.Name, componentName)

		app.printUsage()
	}

	return
}

// usage
func (app *App) printUsage() {

	usage := fmt.Sprintf("Usage:\n\t%s <component> <command> [<args...>]\n\nComponents Available:\n", app.Name)

	for cname := range app.components {
		usage += fmt.Sprintf("\t* %s\n", cname)
	}

	usage += "\n\n"

	log.Printf(usage)
}

// Run start application
func (app *App) Run() (err error) {

	// Add basic components

	app.AddComponent("server", &HTTPServer{Delegate: app})

	err = app.initialize()

	if err != nil {
		log.Printf("[%s][ERROR] %s\n", app.Name, err.Error())
		return
	}

	app.exec()

	return
}

// Router - return a router
func (app *App) Router(host string) *Router {

	if _, hasKey := app.routers[host]; !hasKey {
		app.routers[host] = NewRouter(host)
	}

	return app.routers[host]
}

// DefaultRouter - return a router
func (app *App) DefaultRouter() *Router {

	if _, ok := app.routers["__default__"]; !ok {
		app.routers["__default__"] = NewRouter("__default__")
	}

	return app.routers["__default__"]
}

// AddRoute - add a route to default router
func (app *App) AddRoute(method string, path string, controller interface{}, action string) {

	app.DefaultRouter().AddRoute(method, path, controller, action)
}

// GET - AddRoute("GET")
func (app *App) GET(path string, controller interface{}, action string) {
	app.DefaultRouter().AddRoute("GET", path, controller, action)
}

// POST - AddRoute("POST")
func (app *App) POST(path string, controller interface{}, action string) {
	app.DefaultRouter().AddRoute("POST", path, controller, action)
}

// PUT - AddRoute("PUT")
func (app *App) PUT(path string, controller interface{}, action string) {
	app.DefaultRouter().AddRoute("PUT", path, controller, action)
}

// DELETE - AddRoute("DELETE")
func (app *App) DELETE(path string, controller interface{}, action string) {
	app.DefaultRouter().AddRoute("DELETE", path, controller, action)
}

// HEAD - AddRoute("HEAD")
func (app *App) HEAD(path string, controller interface{}, action string) {
	app.DefaultRouter().AddRoute("HEAD", path, controller, action)
}

// RouteTree - prints all routes tree
func (app *App) RouteTree() {
	for _, router := range app.routers {
		router.RouteTree()
	}
}

// HandleHTTPRequest - implements HTTPServerDelegate
func (app *App) HandleHTTPRequest(w http.ResponseWriter, r *http.Request) {

	ctx := NewContext(app, w, r)

	app.route(ctx)
}

// HandleHTTPError - implements HTTPServerDelegate
func (app *App) HandleHTTPError(err HTTPError) {
	log.Printf("[%s] ERROR: %s\n", app.Name, err.Error())
}

func (app *App) route(ctx *Context) {

	host := ctx.Request.Header.Get("Host")

	if r, ok := app.routers[host]; ok {
		r.Route(ctx)
	} else {
		app.DefaultRouter().Route(ctx)
	}

	return
}
